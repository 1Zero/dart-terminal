import 'package:dart_consolefull/ejercicios/code_wars/palabras_reversa.dart';
import "package:test/test.dart";


void main() {
  test('reversed world', () {
    expect(solution('world'), equals('dlrow'));
  });

  test('hello reversed', () {
    expect(solution('hello'), equals('olleh'));
  });

  test('reversed ', () {
    expect(solution(''), equals(''));
  });

  test('reversed h', () {
    expect(solution('h'), equals('h'));
  });
}
