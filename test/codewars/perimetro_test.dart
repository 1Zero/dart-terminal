import 'package:dart_consolefull/ejercicios/code_wars/perimetro.dart';
import 'package:test/test.dart';

main(){
  group("perimetro", () {
    test('perimetro1', () {
      expect(perimetro(4, 4), equals(16));
      expect(perimetro(6, 10), equals(32));
    });
  });
}