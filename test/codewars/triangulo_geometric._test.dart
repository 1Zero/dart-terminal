import 'package:dart_consolefull/ejercicios/code_wars/triagulo_geometric.dart';
import "package:test/test.dart";


void main() {
  group('Basic tests', () {
    test("otherAngle(30, 60)", () => expect(otherAngle(30, 60), equals(90)));
    test("otherAngle(60, 60)", () => expect(otherAngle(60, 60), equals(60)));
    test("otherAngle(43, 78)", () => expect(otherAngle(43, 78), equals(59)));
    test("otherAngle(10, 20)", () => expect(otherAngle(10, 20), equals(150)));
  });
}
