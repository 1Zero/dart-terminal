import 'package:dart_consolefull/ejercicios/code_wars/array_invertido.dart';
import "package:test/test.dart";


void main() {
  group('basic tests', () {
    test("Testing for [1, 2, 3, 4, 5]",
        () => expect(invertido([1, 2, 3, 4, 5]), equals([-1, -2, -3, -4, -5])));
    test("Testing for [1, -2, 3, -4, 5]",
        () => expect(invertido([1, -2, 3, -4, 5]), equals([-1, 2, -3, 4, -5])));
    test("Testing for []", () => expect(invertido([]), equals([])));
    test("Testing for [0]", () => expect(invertido([0]), equals([0])));
  });
}
