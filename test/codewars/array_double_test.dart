import 'package:dart_consolefull/ejercicios/code_wars/array_double.dart';
import "package:test/test.dart";


void main() {
  group("basic tests", () {
    test("Testing for [1, 2, 3]",
        () => expect(maps1([1, 2, 3]), equals([2, 4, 6])));
    test("Testing for [4, 1, 1, 1, 4]",
        () => expect(maps1([4, 1, 1, 1, 4]), equals([8, 2, 2, 2, 8])));
    test("Testing for [2, 2, 2, 2, 2, 2]",
        () => expect(maps2([2, 2, 2, 2, 2, 2]), equals([4, 4, 4, 4, 4, 4])));
  });
}
