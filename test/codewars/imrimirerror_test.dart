
import 'package:dart_consolefull/ejercicios/code_wars/imprimirerror.dart';
import 'package:test/test.dart';

void main() {
  group('Fixed tests', () {
    test("Testing for aaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbmmmmmmmmmmmmmmmmmmmxyz",
        () {
      expect(
          printerError(
              "aaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbmmmmmmmmmmmmmmmmmmmxyz"),
          equals("3/56"));
    });
    test(
        "Testing for kkkwwwaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbmmmmmmmmmmmmmmmmmmmxyz",
        () {
      expect(
          printerError(
              "kkkwwwaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbmmmmmmmmmmmmmmmmmmmxyz"),
          equals("6/60"));
    });
    test(
        "Testing for kkkwwwaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbmmmmmmmmmmmmmmmmmmmxyzuuuuu",
        () {
      expect(
          printerError(
              "kkkwwwaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbmmmmmmmmmmmmmmmmmmmxyzuuuuu"),
          equals("11/65"));
    });
  });
}
