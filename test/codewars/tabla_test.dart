import 'package:dart_consolefull/ejercicios/code_wars/array_tabla.dart';
import "package:test/test.dart";

void main(){
  group('tablas test',() {
    test('tabla', (){
      expect(tabla(1,10), equals([1,2,3,4,5,6,7,8,9,10]));
    });
    test("tabla(2, 5)", () {
      expect(tabla(2, 5), equals([2, 4, 6, 8, 10]));
    });

    test('tabla2', () {
      expect(tabla2(1, 10), equals([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]));
    });
    test("tabla(2, 5)", () {
      expect(tabla2(2, 5), equals([2, 4, 6, 8, 10]));
    });

    test('tabla3', () {
      expect(tabla3(1, 10), equals([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]));
    });
    test("tabla(2, 5)", () {
      expect(tabla3(2, 5), equals([2, 4, 6, 8, 10]));
    });

    test('tabla4', () {
      expect(tabla4(1, 10), equals([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]));
    });
  });
}