import 'package:dart_consolefull/ejercicios/code_wars/booleano.dart';
import 'package:test/test.dart';

void main(){
  test('boleano',(){
    expect(boleano(true), equals('si'));
    expect(boleano(false), equals('no'));
  });

  test('boleano2',(){
    expect(boleano2(true), equals('Si'));
    expect(boleano2(false), equals('No'));
  });
}



