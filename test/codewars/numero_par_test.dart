import 'package:dart_consolefull/ejercicios/code_wars/numero_par.dart';
import 'package:test/test.dart';

void main() {
  test('numero par', () {
    expect(numeropar(2), equals('Es par'));
    expect(numeropar(1), equals('No es par'));
    expect(numeropar(0), equals('Es par'));
    expect(numeropar(7), equals('No es par'));
    expect(numeropar(-1), equals('No es par'));
  });
}
