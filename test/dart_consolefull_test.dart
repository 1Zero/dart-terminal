import 'package:dart_consolefull/dart_consolefull.dart';
import 'package:dart_consolefull/funciones/nombres.dart';
import 'package:test/test.dart';

void main() {
  test('calculate', () {
    expect(calculate(), 42);
  });

  test('resta',(){
    expect(resta( 6,4),2);
  });

  test('suma',(){
    expect(suma(3,3),6 );
  });

  test('multiplicasion',(){
    expect(multiplicasion(3,3),9);
  });

  test('division',(){
    expect(division(9,3), 3);
  });

  
}

