import 'dart:async';

import 'package:mysql1/mysql1.dart';

class Mysql {
  static var host = 'localhost',
      user = 'root',
      db = 'dart_db',
      password = '123',
      port = 3306;

  Mysql();

  Future<MySqlConnection> getConnection() async {
    var setting = ConnectionSettings(
      host: host,
      port: port,
      user: user,
      password: password,
      db: db,
    );
    return await MySqlConnection.connect(setting);
    
  }

  
}
