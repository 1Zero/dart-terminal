import 'dart:math';

int hexToDec(String hexString) {
  dynamic rez = 0;
  int step = hexString.length - 1;
  for (int i = 0; i < hexString.length; i++) {
    var pow1 = pow(16, step);
    switch (hexString[i]) {
      case '-':
        break;
      case 'a':
      case 'A':
        rez += pow1 * 10;
        break;
      case 'b':
      case 'B':
        rez += pow1 * 11;
        break;
      case 'c':
      case 'C':
        rez += pow1 * 12;
        break;
      case 'd':
      case 'D':
        rez += pow1 * 13;
        break;
      case 'e':
      case 'E':
        rez += pow1 * 14;
        break;
      case 'f':
      case 'F':
        rez += pow1 * 15;
        break;
      default:
        rez += pow1 * int.parse(hexString[i]);
    }
    step--;
  }
  if (hexString[0] == '-') {
    rez *= (-1);
  }
  return rez;
}


int hexToDec1(String hexString) => int.parse(hexString, radix: 16);
