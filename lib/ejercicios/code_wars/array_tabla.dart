

List tabla(int x, int y){
 List<int> answer = [];
  int total = x * y;

  for (int i = x; i <= total; i += x) {
    answer.add(i);
  }

  return answer;
  

}

List<int> tabla2(int c, int x) => List.generate(x, (i) => (i + 1) * c);

List<int> tabla3(int x, int n) {
  return [for (var i = 1; i <= n; i++) x * i];
}

List<int> tabla4(int x, int n) {
  return List.generate(n, (i) => (i + 1) * x);
}
