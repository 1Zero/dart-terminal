
int positiveSum(List<int> arr) {
  return arr.where((l) => l > 0).fold(0, (p, c) => p + c);
}


int positiveSum1(List<int> arr) {
  int total = 0;

  arr.forEach((element) {
    if (element > 0) {
      total = total + element;
    }
  });

  return total;
}
