

List<int> invertido(List<int> arr) {
  return arr.map((e) => -e).toList();
}
