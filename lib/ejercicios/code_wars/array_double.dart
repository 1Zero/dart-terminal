

List<int> maps1(List<int> arr) => arr.map((number) => number * 2).toList();

List<int> maps2(List<int> arr) {
  return arr.map((e) => e * 2).toList();
}
