//Create a function that takes an integer as an argument and returns "Even" for even numbers or "Odd" for odd numbers.

dynamic numeropar(int x){
   if(x % 2 == 0){
     return 'Es par';
   }else{
     return 'No es par';
   }
}

String evenOrOdd(int number) => number.isEven ? 'Even' : 'Odd';

String evenOrOdd2(int n) => n % 2 == 0 ? 'Even' : 'Odd';
