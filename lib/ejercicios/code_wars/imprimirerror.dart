final RegExp reError = new RegExp("[^a-m]");

String printerError(String s) {
  return "${reError.allMatches(s).length}/${s.length}";
}
