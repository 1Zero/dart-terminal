
// You are given the length and width of a 4-sided polygon. The polygon can either be a rectangle or a square.
// If it is a square, return its area. If it is a rectangle, return its perimeter.

 int  perimetro( int x, int y)  {
     if (x == y) {
        return x * y;
    } else {
        return 2 * (x + y);
    }
}

// ignore: prefer_function_declarations_over_variables
var perimetro2 = (l, w) => l != w ? (l + w) * 2 : l * w;
