// parámetros posicionales

int suma(int sumando1, int sumando2) {
  // print('ingresa el primer numero');
  //  int sumando1 = int.parse(stdin.readLineSync()!);
  //   print('ingresa el segundo numero');
  // int sumando2 = int.parse(stdin.readLineSync()!);
  return sumando1 + sumando2;
}
int resta(int resta1, int resta2){
  return resta1-resta2;
}

int multiplicasion(int mult1, int mult2){
  return mult1 * mult2;
}

double division (double div1, double div2){
  return div1 / div2;
}

// parámetros posicionales
String saludo(String nombre, String sexo) {
  switch (sexo) {
    case 'femenino':
      return 'Bienvenida $nombre';
    case 'masculino':
      return 'Bienvenido $nombre';
    default:
      return 'Hola $nombre';
  }
}


int  triangular(int n) {
  var sum = 0;
  while (n > 0) {
    sum += n--;
  }

  return sum;
}

// parámetros por nombre
String saludo2({required String nombre, String sexo = 'prefiero no decirlo'}) {
  switch (sexo) {
    case 'femenino':
      return 'Bienvenida $nombre';
    case 'masculino':
      return 'Bienvenido $nombre';
    default:
      return 'Hola $nombre';
  }
}


void main(List<String> args) {
  print(suma(3, 5));
  // print(saludo('Maria', 'femenino'));
  // print(saludo('Mario', 'masculino'));
  // print(saludo('Andra', 'prefiero no decirlo'));
  print(saludo2(nombre: 'Andra'));
  print(saludo2(nombre: 'Maria', sexo: 'femenino'));
  print(saludo2(sexo: 'femenino', nombre: 'Maria'));
  print(saludo2(nombre: 'Benito', sexo: 'masculino'));
}
